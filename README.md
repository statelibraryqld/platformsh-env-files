# platform.sh env files

writes platform.sh env vars whose name starts with `file:` to the named file, similarly to how `d8config:` is used to set drupal 8 config

eg. `'file:/tmp/path' = 'data to write\n2nd line'`

## prerequisites

node.js 12+ is required in the p.sh container

## setup

[install / update node](https://docs.platform.sh/languages/nodejs/nvm.html) if required  (eg. the php image has node but only v6):

then run the package from the build or deploy script in `.platform.app.yaml` using `npx`:

```yaml
hooks:
  deploy: |
    npx platformsh-env-files
```

alternatively, if running only at deploy, it can be preinstalled into the image to reduce deploy runtime:

```sh
npm i platformsh-env-files
```

if writing files at deploy, remember that most directories are read-only by default so make sure to choose a writeable location
