#!/usr/bin/env node

const fs = require('fs').promises

const conf = require('platformsh-config').config()
if (!conf.isValidPlatform()) {
  console.error('error: run on platform.sh')
  process.exit(1)
}

console.log('writing platform.sh env files')

const vars: { readonly [i: string]: string } = conf.variables()

Promise.allSettled(
  Object.entries(vars).map(async ([key, val]) => {
    if (key.startsWith('file:'))
      await fs.writeFile(key.slice(5), typeof val === 'object' ? JSON.stringify(val) : val, 'utf-8')
  })
).catch(err => {
  console.error(err)
  process.exit(1)
})
